/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras;

import java.util.ArrayList;

/**
 *
 * @author matt
 */
public class Tupla<T1,T2> {
    private T1 primElem;
    private T2 secElem;
    
    public Tupla(T1 primElem, T2 secElem){
        this.primElem = primElem;
        this.secElem = secElem;
    }
    
    public T1 getPrimElem(){
        return this.primElem;
    }
    
    public T2 getSecElem(){
        return this.secElem;
    }
    
    public void setPrimElem(T1 unElem){
        this.primElem = unElem;
    }
    
    public void setSecElem(T2 unElem){
        this.secElem = unElem;
    }
}
