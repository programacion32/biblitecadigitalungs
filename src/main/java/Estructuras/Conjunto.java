/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras;

import java.util.ArrayList;

/**
 *
 * @author matt
 */
public class Conjunto<T> {

    private ArrayList<T> elementos = new ArrayList<T>();

    public Integer getTamanio() {
        return elementos.size();
    }

    public void agregar(T elem) {
        if (!existe(elem) || getTamanio() == 0) {
            elementos.add(elem);
        }
    }

    private boolean existe(T elem) {
        boolean control = false;
        for (int i = 0; i < getTamanio(); i++) {
            if (elementos.get(i).equals(elem)) {
                control = control | true;
            }
        }
        return control;
    }

    public T getElemento(Integer i) {
        return elementos.get(i);
    }

    public void union(Conjunto<T> conjOrigen) {
        for (int i = 0; i < conjOrigen.getTamanio(); i++) {
            if (!existe(conjOrigen.getElemento(i))) {
                elementos.add(conjOrigen.getElemento(i));
            }
        }
    }

    public void interseccion(Conjunto<T> conjOrigen) {
        Conjunto<T> intersecc = new Conjunto<T>();
        for (int i = 0; i < conjOrigen.getTamanio(); i++) {
            if (existe(conjOrigen.getElemento(i))) {
                intersecc.agregar(conjOrigen.getElemento(i));
            }
        }
        elementos = null;
        elementos = intersecc.getArray();
    }

    private ArrayList getArray() {
        return elementos;
    }

    public String print() {
        String cadena = "";
        for (int i = 0; i < getTamanio(); i++) {
            cadena = cadena + ", " + getElemento(i);
        }
        return cadena;
    }

}
