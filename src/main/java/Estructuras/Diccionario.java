/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras;

import Estructuras.Conjunto;
import java.util.ArrayList;

/**
 *
 * @author matt
 */
public class Diccionario<K, V> {

    private Conjunto<Tupla> conj = new Conjunto<Tupla>();

    public void putElemento(K key, V elem) {
        Tupla temp = new Tupla(key, elem);
        if (existe(key)) {
            conj.getElemento(getPos(key)).setSecElem(elem);            
        }else{
            conj.agregar(temp);
        }

    }

    private boolean existe(K key) {
        boolean exi = false;
        for (int i = 0; i < conj.getTamanio(); i++) {
            if (conj.getElemento(i).getPrimElem().equals(key)) {
                exi = exi | true;
            }
        }
        return exi;
    }
    
    private int getPos(K key) {
        int pos = 0;
        for (int i = 0; i < conj.getTamanio(); i++) {
            if (conj.getElemento(i).getPrimElem().equals(key)) {
                pos = i;
            }
        }
        return pos;
    }

    public V getValue(K key) {
        for (int i = 0; i < conj.getTamanio(); i++) {
            if (key.equals(conj.getElemento(i).getPrimElem())) {
                return (V) conj.getElemento(i).getSecElem();
            }
        }
        return null;
    }
    
    public void setKey(){
        
    }

}
