/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.malpuin.bibliotecadigitalungs;

/**
 *
 * @author matt
 */
public class Libro {

    /*
        ->codigo ISBN(codigo ejemplar del libro)
        ->categoria
        ->nombre
        ->ancho
        *constructor LIBRO("codISBN","categoria","nombre","ancho")
        *boolen equals(this categoria, this nombre, this ancho)
    
     */
    private String codISBN;
    private String categoria;
    private String nombre;
    private int ancho;

    public Libro(String codISBN, String categoria, String nombre, int ancho) {
        this.codISBN = codISBN;
        this.categoria = categoria;
        this.nombre = nombre;
        this.ancho = ancho;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final Libro other = (Libro) obj;

        if (this.codISBN == other.getCodISBN()
                && this.categoria == other.getCategoria()
                && this.nombre == other.getNombre()
                && this.ancho == other.getAncho()) {
            return true;
        } else {
            return false;
        }

    }

    public String getCodISBN() {
        return codISBN;
    }

    public void setCodISBN(String codISBN) {
        this.codISBN = codISBN;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getAncho() {
        return ancho;
    }

    public void setAncho(int ancho) {
        this.ancho = ancho;
    }

}
