/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.malpuin.bibliotecadigitalungs;

/**
 *
 * @author matt
 */
public class BibliotecaDigitalUNGS {

    public static void main(String args[]) {
        BDUNGS bd = new BDUNGS(10, 100);
        bd.rotularEstante("​Computacion​", 1);
        bd.rotularEstante("​Matematica​", 2);
        bd.rotularEstante("​Computacion​", 3);
        
        bd.ingresarLibro("​9789684443457​", "​Computacion​", "​Estructuras de datos", 5);
        bd.ingresarLibro("​9788415552222​", "​Computacion​", "Java Principiantes​", 7);
        bd.ingresarLibro("​9389557783457​", "​Matematica​", "​Analisis de Funciones​", 4);
        
        System.out.println(bd);
        bd.eliminarLibro("​9389557783457​");
        bd.rotularEstante("​Análisis Matemático​", 2);
        //System.out.println(bd);

    }
}
