/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.malpuin.bibliotecadigitalungs;

import Estructuras.Diccionario;
import java.util.ArrayList;
import java.util.Iterator;
import org.w3c.dom.CDATASection;

/**
 *
 * @author matt
 */
public class Estante {

    /*
        ->ancho
        ->categoria
        ->numOrden //1 al 10
        ->DICC<"codISBN",LIBRO> libros //cod libro por estante //no se si sirve
        ->ArrayList<LIBRO> libros
        ->cantLibros //libros por estante, al iniciar el constructor va en cero
        ->espacioVacio //cuando se inicia es igual al ancho, y a medida que se ingresan libros se le resta 
        *rotular() //solo si esta vacio
        *elimnarLibro()  //el libro sale de la biblioteca, se iliminan todos los ejemplares correspondientes al ISBN)
        *reacomodarLibros()
        *verLlibros()
        *constructor ESTANTE(numOrden, ancho)
        *agregarLibro() //agrega libro en libros, quita ancho de espacioVacio
     */
    private int numOrden;
    private int ancho;
    private String categoria;//probablemente tenga que usar un string builder
    private ArrayList<Libro> libros; //si esta en cero, el estante esta vacio ; si tiene algo su tamaño corresponde a la cantidad de libros
    private int espacioVacioPendiente; //inicialmente es igual al ancho del estante, a medida que entran libros se le reduce el estacio vacio

    public Estante(int numOrden, int ancho) {
        this.numOrden = numOrden;
        this.ancho = ancho;
        this.categoria = "";
        this.espacioVacioPendiente = this.ancho;
        this.libros = new ArrayList<Libro>();
    }

    public boolean rotular(String categoria) {
        boolean resultado = false;
        try {
            if (libros.size() == 0) {
                this.categoria = categoria;
                resultado = true;
            }
        } catch (Exception e) {
            System.out.println("Error en Estante().Rotular(String categoria):" + e);
        } finally {
            return resultado;
        }
    }

    public boolean agregarLibro(Libro libro) {
        boolean resultado = false;
        try {
            if (this.espacioVacioPendiente >= libro.getAncho()) {
                libros.add(libro);
                this.espacioVacioPendiente -= libro.getAncho();
                //System.out.println("Se agrego un libro: " + libro.getNombre());
                resultado = true;
            }
        } catch (Exception e) {
            System.out.println("Error en Estante().agergarLibro(Libro libro):" + e);
        } finally {
            return resultado;
        }
    }

    public void eliminarLibros(String codISBN) {
        try {
            Iterator it = libros.iterator();
            while (it.hasNext()) {
                Libro libro = (Libro) it.next();
                if (libro.getCodISBN().equals(codISBN)) {
                    it.remove();
                }
            }
        } catch (Exception e) {
            System.out.println("Error en Estante().eliminarLibro(Libro libro):" + e);
        }
    }

    @Override
    public String toString() {
        StringBuilder salida = new StringBuilder();
        Iterator it = libros.iterator();
        while (it.hasNext()) {
            Libro libro = (Libro) it.next();
            salida.append("* ");
            salida.append(libro.getCodISBN() + "\t");
            salida.append(libro.getNombre() + "\t");
            salida.append(libro.getCategoria() + "\t");
            salida.append(libro.getAncho() + "\n");
        }

        return salida.toString();
    }

    public String getCategoria() {
        return categoria;
    }

    public int getNumOrden() {
        return numOrden;
    }

    public int getEspacioVacioPendiente() {
        return espacioVacioPendiente;
    }

    public ArrayList<Libro> getLibros() {
        return libros;
    }

    public int getAncho() {
        return ancho;
    }

}
