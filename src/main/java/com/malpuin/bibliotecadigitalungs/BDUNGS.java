/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.malpuin.bibliotecadigitalungs;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author matt
 */
public class BDUNGS {/*
        ->DICC<numOrd,ESTANTE> estantes //
        ->DICC<"codISBN",LIBRO> librosExistentes //
        *constructor BDUNGS(cantEstantes,anchoEstantes) //
        *rotularEstante("categoria", numOrden) //solo si esta vacio
        *ingresarLibro("codISBN","categoria","nombre",ancho) //no tiene que estar en librosExistentes
        *eliminarLibro("codigoISBN") //busca libros en existentes y en cada estante y los elimina
     */
    private Map<Integer, Estante> estantes;
    //private Diccionario<String, Libro> librosExistentes;

    public BDUNGS(Integer numEstantes, Integer ancho) {
        this.estantes = new HashMap<>();
        //this.librosExistentes = new Diccionario<>();

        for (int i = 1; i <= numEstantes; i++) {
            this.estantes.put(i, new Estante(i, ancho));
        }
    }

    public void rotularEstante(String categoria, Integer numOrden) {
        System.out.println("*******************************************************");
        System.out.println("Estante N°: " + numOrden);
        if (estantes.get(numOrden).rotular(categoria)) {
            System.out.println("Se rotulo correctamente");
        } else {
            System.out.println("No se puede rotular porque no esta vacio");
        }
    }

    public void ingresarLibro(String codISBN, String categoria, String nombre, Integer ancho) {
        boolean error = false;
        try {
            Libro libroTemp = new Libro(codISBN, categoria, nombre, ancho);
            boolean sinAgregar = true;
            for (Integer numEstante : estantes.keySet()) {

                if (estantes.get(numEstante).getCategoria() != null && sinAgregar) {
                    if (estantes.get(numEstante).getCategoria().equals(libroTemp.getCategoria()) && libroTemp.getAncho() <= estantes.get(numEstante).getEspacioVacioPendiente()) {
                        estantes.get(numEstante).agregarLibro(libroTemp);
                        sinAgregar = false;
                        //mostrarInfo(libroTemp, numEstante);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Error en BDUNGS().ingresarLibro(String codISBN, String categoria, String nombre, Integer ancho):" + e);
            error = true;
        } finally {
            if (error) {
                System.out.println("Error al ingresar libro");
            }
        }
    }

    public void eliminarLibro(String codISBN) {
        try {
            for (Integer numEstante : estantes.keySet()) {

                if (estantes.get(numEstante).getLibros().size() > 0) {
                    estantes.get(numEstante).eliminarLibros(codISBN);
                }
            }
        } catch (Exception e) {
            System.out.println("Error en BDUNGS().buscarCategoria(String categoria, Libro libro):" + e);
        }
    }

    public Integer espacioLibre(Integer numEstante) {
        return estantes.get(numEstante).getEspacioVacioPendiente();
    }

    private void mostrarInfo(Libro libro, Integer numEstante) {
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("Hay espacio en Estante n°:" + numEstante);
        System.out.println("-------------------------------------------------------");
        System.out.println("Se inserta libro:" + libro.getNombre());
        System.out.println("El estante es categoria:" + estantes.get(numEstante).getCategoria());
        System.out.println("La categoria del libro es:" + libro.getCategoria());
        System.out.println("El ancho disponible en estante:" + (estantes.get(numEstante).getEspacioVacioPendiente() + libro.getAncho()));
        System.out.println("El ancho del libro es:" + libro.getAncho());
        System.out.println("Espacio en estante:" + estantes.get(numEstante).getEspacioVacioPendiente());
    }

    @Override
    public String toString() {
        StringBuilder salida = new StringBuilder();

        for (Integer numEstante : estantes.keySet()) {
            salida.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
            salida.append("Estante N°" + numEstante + "\n");
            salida.append("Espacio Categoria:" + estantes.get(numEstante).getCategoria() + "\n");
            salida.append("Cant Libros:" + estantes.get(numEstante).getLibros().size() + "\n");
            salida.append("Espacio libre:" + estantes.get(numEstante).getEspacioVacioPendiente() + "\n");
            salida.append("Espacio ocupado:" + (estantes.get(numEstante).getAncho() - estantes.get(numEstante).getEspacioVacioPendiente()) + "\n");
            salida.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
            salida.append("Libros:\n");            
            salida.append(estantes.get(numEstante).toString());
            salida.append("-------------------------------------------------------\n");
        }
        return salida.toString();
    }

}
